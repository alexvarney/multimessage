﻿namespace MultiMessage
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.rBtnClient = new System.Windows.Forms.RadioButton();
            this.rBtnServer = new System.Windows.Forms.RadioButton();
            this.txtIPAddress = new System.Windows.Forms.TextBox();
            this.txtPort = new System.Windows.Forms.TextBox();
            this.btnStart = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.gbConnections = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtUsername = new System.Windows.Forms.TextBox();
            this.rtbInput = new System.Windows.Forms.RichTextBox();
            this.btnSend = new System.Windows.Forms.Button();
            this.rtbMessages = new System.Windows.Forms.RichTextBox();
            this.gbConnections.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(70, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Select Mode:";
            // 
            // rBtnClient
            // 
            this.rBtnClient.AutoSize = true;
            this.rBtnClient.Checked = true;
            this.rBtnClient.Location = new System.Drawing.Point(81, 19);
            this.rBtnClient.Name = "rBtnClient";
            this.rBtnClient.Size = new System.Drawing.Size(51, 17);
            this.rBtnClient.TabIndex = 1;
            this.rBtnClient.TabStop = true;
            this.rBtnClient.Text = "Client";
            this.rBtnClient.UseVisualStyleBackColor = true;
            this.rBtnClient.CheckedChanged += new System.EventHandler(this.rBtnClient_CheckedChanged);
            // 
            // rBtnServer
            // 
            this.rBtnServer.AutoSize = true;
            this.rBtnServer.Location = new System.Drawing.Point(138, 19);
            this.rBtnServer.Name = "rBtnServer";
            this.rBtnServer.Size = new System.Drawing.Size(56, 17);
            this.rBtnServer.TabIndex = 2;
            this.rBtnServer.Text = "Server";
            this.rBtnServer.UseVisualStyleBackColor = true;
            // 
            // txtIPAddress
            // 
            this.txtIPAddress.Location = new System.Drawing.Point(9, 69);
            this.txtIPAddress.Name = "txtIPAddress";
            this.txtIPAddress.Size = new System.Drawing.Size(143, 20);
            this.txtIPAddress.TabIndex = 3;
            // 
            // txtPort
            // 
            this.txtPort.Location = new System.Drawing.Point(158, 69);
            this.txtPort.Name = "txtPort";
            this.txtPort.Size = new System.Drawing.Size(57, 20);
            this.txtPort.TabIndex = 4;
            // 
            // btnStart
            // 
            this.btnStart.Location = new System.Drawing.Point(221, 67);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(63, 23);
            this.btnStart.TabIndex = 5;
            this.btnStart.Text = "Connect";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 92);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(58, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "IP Address";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(157, 92);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(26, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Port";
            // 
            // gbConnections
            // 
            this.gbConnections.Controls.Add(this.label4);
            this.gbConnections.Controls.Add(this.txtUsername);
            this.gbConnections.Controls.Add(this.label1);
            this.gbConnections.Controls.Add(this.label3);
            this.gbConnections.Controls.Add(this.rBtnClient);
            this.gbConnections.Controls.Add(this.label2);
            this.gbConnections.Controls.Add(this.rBtnServer);
            this.gbConnections.Controls.Add(this.btnStart);
            this.gbConnections.Controls.Add(this.txtIPAddress);
            this.gbConnections.Controls.Add(this.txtPort);
            this.gbConnections.Location = new System.Drawing.Point(12, 12);
            this.gbConnections.Name = "gbConnections";
            this.gbConnections.Size = new System.Drawing.Size(290, 124);
            this.gbConnections.TabIndex = 8;
            this.gbConnections.TabStop = false;
            this.gbConnections.Text = "Connections";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(9, 45);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(55, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "Username";
            // 
            // txtUsername
            // 
            this.txtUsername.Location = new System.Drawing.Point(70, 43);
            this.txtUsername.Name = "txtUsername";
            this.txtUsername.Size = new System.Drawing.Size(145, 20);
            this.txtUsername.TabIndex = 8;
            // 
            // rtbInput
            // 
            this.rtbInput.Location = new System.Drawing.Point(12, 392);
            this.rtbInput.Name = "rtbInput";
            this.rtbInput.Size = new System.Drawing.Size(243, 41);
            this.rtbInput.TabIndex = 10;
            this.rtbInput.Text = "";
            this.rtbInput.KeyDown += new System.Windows.Forms.KeyEventHandler(this.rtbInput_KeyDown);
            // 
            // btnSend
            // 
            this.btnSend.Location = new System.Drawing.Point(261, 392);
            this.btnSend.Name = "btnSend";
            this.btnSend.Size = new System.Drawing.Size(41, 41);
            this.btnSend.TabIndex = 11;
            this.btnSend.Text = "Send";
            this.btnSend.UseVisualStyleBackColor = true;
            this.btnSend.Click += new System.EventHandler(this.btnSend_Click);
            // 
            // rtbMessages
            // 
            this.rtbMessages.Location = new System.Drawing.Point(12, 142);
            this.rtbMessages.Name = "rtbMessages";
            this.rtbMessages.ReadOnly = true;
            this.rtbMessages.Size = new System.Drawing.Size(290, 244);
            this.rtbMessages.TabIndex = 12;
            this.rtbMessages.Text = "";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(314, 445);
            this.Controls.Add(this.rtbMessages);
            this.Controls.Add(this.btnSend);
            this.Controls.Add(this.rtbInput);
            this.Controls.Add(this.gbConnections);
            this.Name = "Form1";
            this.Text = "MultiMessage";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.gbConnections.ResumeLayout(false);
            this.gbConnections.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RadioButton rBtnClient;
        private System.Windows.Forms.RadioButton rBtnServer;
        private System.Windows.Forms.TextBox txtIPAddress;
        private System.Windows.Forms.TextBox txtPort;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox gbConnections;
        private System.Windows.Forms.RichTextBox rtbInput;
        private System.Windows.Forms.Button btnSend;
        private System.Windows.Forms.RichTextBox rtbMessages;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtUsername;


    }
}

