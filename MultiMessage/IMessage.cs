﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MultiMessage
{
    interface IMessage
    {
        event EventHandler<ConnectionStateChangeEventArgs> ConnectionStateChanged;
        event EventHandler<MessageRecievedEventArgs> MessageRecieved;

        void SendMessage(string data);

    }
}
