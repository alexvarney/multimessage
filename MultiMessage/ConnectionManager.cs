﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.Diagnostics;

namespace MultiMessage
{
    class ConnectionManager
    {
        private enum ProcessingState 
        { 
            Processing,
            Complete
        }

        public event EventHandler<ConnectionStateChangeEventArgs> ConnectionStateChange;
        public event EventHandler<MessageRecievedEventArgs> MessageRecieved;
        //public event EventHandler

        private readonly int port; //port to connect / listen on

        private TcpClient Client;
        private TcpListener Listener;
        private NetworkStream ClientStream;

        private string data;
        private byte[] dataBuffer = new byte[1];

        private Queue<string> messageQueue = new Queue<string>();
        ProcessingState messageQueueProcessingState = ProcessingState.Complete;


        public ConnectionManager(int port) {

            this.port = port;
            
        }

        //connect to a specific client
        public void EstablishConnection(IPAddress target) 
        {
            Client = new TcpClient();

            try
            {
                Client.Connect(target, this.port);
            } 
            catch 
            {
                OnConnectionStateChange(new ConnectionStateChangeEventArgs() { NewConnectionState = ConnectionStateChangeEventArgs.ConnectionState.Disconnected });
                return; //avoids firing "connected" event
            }
            ClientStream = Client.GetStream();
            RecieveBytes();
            OnConnectionStateChange(new ConnectionStateChangeEventArgs() { NewConnectionState = ConnectionStateChangeEventArgs.ConnectionState.Connected});
        }

        //start listening for a connection
        public void AcceptConnection() 
        {
            Listener = new TcpListener(IPAddress.Parse("0.0.0.0"),this.port);
            Listener.Start();
            Listener.BeginAcceptTcpClient(new AsyncCallback(ListenerConnected), null);
        }
        
        //accept a client
        private void ListenerConnected(IAsyncResult result)
        {
            this.Client = Listener.EndAcceptTcpClient(result);
            ClientStream = Client.GetStream();
            RecieveBytes();
            OnConnectionStateChange(new ConnectionStateChangeEventArgs() { NewConnectionState = ConnectionStateChangeEventArgs.ConnectionState.Connected });
        }

        //fire the ConnectionStateChange event
        protected virtual void OnConnectionStateChange(ConnectionStateChangeEventArgs e) {
            EventHandler<ConnectionStateChangeEventArgs> handler = ConnectionStateChange;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        //start accepting a message
        private void RecieveBytes()
        {
            ClientStream.BeginRead(dataBuffer, 0, 1, new AsyncCallback(DataRecieved), null);
        }

        //process a message
        private void DataRecieved(IAsyncResult result) 
        {
            data += Encoding.Default.GetString(dataBuffer);
            dataBuffer = new byte[1];

            Debug.WriteLine(data);
            
            if(data.IndexOf("\n") > -1)
            {
                
                char[] splitChar = {'\n'};
                string[] messageSplit = data.Split(splitChar);
                data = messageSplit[1];
                OnMessageRecieved(new MessageRecievedEventArgs { Message = messageSplit[0] });
            }

            RecieveBytes(); //start recieving the next block

        }
        
        //event for when a message is recieved
        protected virtual void OnMessageRecieved(MessageRecievedEventArgs e)
        {
            EventHandler<MessageRecievedEventArgs> handler = MessageRecieved;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        public void SendMessage(String message)
        {
            messageQueue.Enqueue(message);
        }

        private void ProcessQueue() 
        { 
            
        }

    }
}
