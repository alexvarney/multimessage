﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MultiMessage
{
    class ConnectionStateChangeEventArgs : EventArgs
    {
        public enum ConnectionState
        {
            Connected,
            Disconnected,
            Unknown
        }

        public ConnectionState NewConnectionState;

    }
}
