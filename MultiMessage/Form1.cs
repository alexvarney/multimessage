﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;

namespace MultiMessage
{
    public partial class Form1 : Form
    {
        enum Mode 
        { 
            Client,
            Server
        }

        Mode ApplicationMode = Mode.Client;
        IMessage connection;

        bool DebugMode = true;

        public Form1()
        {
            InitializeComponent();

        }

        private void rBtnClient_CheckedChanged(object sender, EventArgs e)
        {
            if (rBtnClient.Checked)
            {
                EnterClientMode();
            }
            else
            {
                EnterServerMode();
            }
        }

        private void EnterClientMode()
        {
            ApplicationMode = Mode.Client;
            txtIPAddress.Enabled = true;
            btnStart.Text = "Connect";
            txtIPAddress.Text = "";
            txtUsername.Enabled = true;
            txtUsername.Text = "USER";
        }

        private void EnterServerMode()
        {
            ApplicationMode = Mode.Server;
            txtIPAddress.Enabled = false;
            btnStart.Text = "Start";
            txtIPAddress.Text = "0.0.0.0";
            txtUsername.Enabled = false;
            txtUsername.Text = "SERVER";
        }

        private void AddMessageToDisplay(Message m)
        {
            rtbMessages.Invoke(new Action(() => {

                rtbMessages.SelectionFont = new Font(rtbMessages.Font, FontStyle.Bold);
                rtbMessages.AppendText(m.Username + ": ");
                rtbMessages.SelectionFont = new Font(rtbMessages.Font, FontStyle.Regular);
                rtbMessages.AppendText(m.Data + '\n');
            
            }));
        }

        private void AddMessageToDisplay(string message)
        {
            rtbMessages.Invoke(new Action(() => rtbMessages.Text += message + '\n'));
        }

        private void btnSend_Click(object sender, EventArgs e)
        {
            connection.SendMessage(rtbInput.Text);
            AddMessageToDisplay(new Message() { Username = this.txtUsername.Text, Data = rtbInput.Text });
            rtbInput.Clear();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            rtbMessages.BackColor = Color.White;
            if (DebugMode == true)
            {
                this.txtUsername.Text = "alexvarney";
                this.txtPort.Text = "2222";
                this.txtIPAddress.Text = "127.0.0.1";
            }
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            if (this.ApplicationMode == Mode.Client)
            {
                connection = new ClientConnection("alexvarney");
                connection.ConnectionStateChanged += Connection_StateChange;

                IPAddress IPAddr;
                int Port;

                if (IPAddress.TryParse(txtIPAddress.Text, out IPAddr) == false)
                {
                    MessageBox.Show("Invalid IP Address");
                    return;
                }

                if (Int32.TryParse(txtPort.Text, out Port) == false)
                {
                    MessageBox.Show("Invalid Port Number");
                    return;
                }
                var client = (ClientConnection)connection;
                client.BeginEstablishConnection(IPAddr, Port);
            }
            if (this.ApplicationMode == Mode.Server)
            {
                connection = new ServerConnectionManager();
                var server = (ServerConnectionManager)connection;

                int port;

                if (Int32.TryParse(txtPort.Text, out port) == false)
                {
                    MessageBox.Show("Invalid Port Number");
                    return;
                }
                server.MessageRecieved += MessageRecieved_Handler;
                server.AcceptConnections(port);
            }
        }

        private void Connection_StateChange(object sender, ConnectionStateChangeEventArgs e)
        {
            if (e.NewConnectionState == ConnectionStateChangeEventArgs.ConnectionState.Connected)
            {
                AddMessageToDisplay(new Message() { Username = "Info", Data = "Connected" });
                connection.MessageRecieved += MessageRecieved_Handler;
            }
            else
            {
                AddMessageToDisplay(new Message() { Username = "Info", Data = "Not Connected" });
            }
        }

        private void MessageRecieved_Handler(object sender, MessageRecievedEventArgs e)
        {
            AddMessageToDisplay(e.Message);
        }

        private void rtbInput_KeyDown(object sender, KeyEventArgs e)
        {   //send message on press of enter
            if (e.KeyData == Keys.Enter)
            {
                e.Handled = true;
                connection.SendMessage(rtbInput.Text);
                AddMessageToDisplay(new Message() { Username = this.txtUsername.Text, Data = rtbInput.Text });
                rtbInput.Clear();
            }
        }

    }
}
