﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MultiMessage
{
    class Message
    {
        public readonly string Action = "message";
        public string Username = "";
        public string Data = "";
    }
}
