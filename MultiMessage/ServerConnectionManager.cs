﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;

namespace MultiMessage
{
    class ServerConnectionManager : IMessage
    {
        //Private Declarations
        readonly string Username = "SERVER";
        private List<ServerConnection> clients = new List<ServerConnection>();
        private TcpListener tcpListener;

        //Public Events
        public event EventHandler<ConnectionStateChangeEventArgs> ConnectionStateChanged;
        public event EventHandler<MessageRecievedEventArgs> MessageRecieved;

        //Event Handlers
        protected virtual void OnConnectionStateChange(ConnectionStateChangeEventArgs e)
        {
            if (ConnectionStateChanged != null)
            {
                ConnectionStateChanged(this, e);
            }
        }

        protected virtual void OnMessageRecieved(MessageRecievedEventArgs e)
        {
            if (MessageRecieved != null)
            {
                MessageRecieved(this, e);
            }
        }

        //Public Methods
        public void SendMessage(string data)
        {
            foreach (var connection in clients)
            {
                connection.SendMessage(new Message() { Username = this.Username, Data = data });
            }
        }

        public void AcceptConnections(int port)
        {
            tcpListener = new TcpListener(IPAddress.Parse("0.0.0.0"), port);
            tcpListener.Start();
            BeginAcceptNewConnection();
        }
        
        //Private Methods
        private void BeginAcceptNewConnection() 
        {
            tcpListener.BeginAcceptTcpClient(CompleteConnectionAcceptance, null);
        }

        private void CompleteConnectionAcceptance(IAsyncResult result)
        {
            var newClient = tcpListener.EndAcceptTcpClient(result);
            var newServerConnection = new ServerConnection(newClient);
            newServerConnection.MessageRecieved += ConnectionMessageRecieved;
            clients.Add(newServerConnection);

            BeginAcceptNewConnection();
        }

        private void ConnectionMessageRecieved(object sender, MessageRecievedEventArgs e)
        {
            foreach (var connection in clients)
            {
                if (connection != sender)
                {
                    connection.SendMessage(e.Message);
                }
            }

            OnMessageRecieved(e);

        }
    }
}
