﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.Diagnostics;
using Newtonsoft.Json;

namespace MultiMessage
{
    class ServerConnection : IMessage
    {
        //Private Declarations
        private TcpClient connection;
        private NetworkStream stream;
        private string incomingData = "";
        private const char endMessageChar = '\u0003';

        //Public Events
        public event EventHandler<ConnectionStateChangeEventArgs> ConnectionStateChanged;
        public event EventHandler<MessageRecievedEventArgs> MessageRecieved;

        //Event Handlers
        protected virtual void OnConnectionStateChange(ConnectionStateChangeEventArgs e)
        {
            if (ConnectionStateChanged != null)
            {
                ConnectionStateChanged(this, e);
            }
        }

        protected virtual void OnMessageRecieved(MessageRecievedEventArgs e)
        {
            if (MessageRecieved != null)
            {
                MessageRecieved(this, e);
            }
        }

        //Public Methods
        public ServerConnection(TcpClient client)
        {
            connection = client;
            stream = client.GetStream();
            BeginRecieveByte();
        }
        public void SendMessage(string data)
        {
            SendMessage(new Message() { Username = "unknown", Data = data });
        }

        public void SendMessage(Message m)
        {
            var jsonData = JsonConvert.SerializeObject(m, Formatting.None);

            Byte[] messageBytes = Encoding.UTF8.GetBytes(jsonData + '\u0003');

            if (IsConnected(connection) == false)
            {
                OnConnectionStateChange(new ConnectionStateChangeEventArgs() { NewConnectionState = ConnectionStateChangeEventArgs.ConnectionState.Disconnected });
                return;
            }

            stream.BeginWrite(messageBytes, 0, messageBytes.Length, CompleteWriteMessage, null);
        }

        //Private Methods

        private void CompleteWriteMessage(IAsyncResult result) { }

        private bool IsConnected(TcpClient tcp)
        {
            // Detect if client disconnected
            if (tcp.Client.Poll(0, SelectMode.SelectRead))
            {
                byte[] buff = new byte[1];
                if (tcp.Client.Receive(buff, SocketFlags.Peek) == 0)
                {
                    // Client disconnected
                    return false;
                }
            }
            return true;
        }

        private void BeginRecieveByte()
        {   //recieve a new byte
            byte[] newByte = new byte[1];

            if (IsConnected(connection))
            {
                stream.BeginRead(newByte, 0, 1, new AsyncCallback(EndRecieveByte), newByte);
            }
            else
            {
                OnConnectionStateChange(new ConnectionStateChangeEventArgs() { NewConnectionState = ConnectionStateChangeEventArgs.ConnectionState.Disconnected });
            }
        }

        private void EndRecieveByte(IAsyncResult result)
        {   //send that byte off for processing
            byte[] newByte = (byte[])result.AsyncState;
            Debug.Write(Encoding.UTF8.GetString(newByte));

            incomingData += Encoding.UTF8.GetString(newByte);

            ProcessIncomingData(ref incomingData);

            BeginRecieveByte();
        }

        private void ProcessIncomingData(ref string data)
        { //see if a completed message has been received and if so, fire the MessageRecieved event with the message data
            var index = data.IndexOf(endMessageChar);

            if (data.IndexOf(endMessageChar) > -1)
            {
                try
                {
                    Message recievedMessage = JsonConvert.DeserializeObject<Message>(data.Substring(0, data.Length -1));
                    data = "";
                    OnMessageRecieved(new MessageRecievedEventArgs { Message = recievedMessage });
                }
                catch
                { 
                
                }
            }
        }

    }
}
